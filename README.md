<!--
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: 2023 KDE e.V.
# SPDX-FileContributor: Ingo Klöcker <dev@ingo-kloecker.de>
-->

# microstore

This is a simple Python module for the Microsoft Store submission API.

## Installation

```
pip3 install git+https://invent.kde.org/kloecker/microstore.git
```

## Usage

### Using the Client

microstore features a simple command line client providing a few commands to interact with the Microsoft Store submission API. Run
```
microstore --help
```
for details.

To simplify running the client you may want to add a `.env` file which exports a few environment variables, so that you do not have to specify them on the command line, e.g.
```
export MICROSTORE_TENANT_ID="<your tenant ID>"
export MICROSTORE_CLIENT_ID="<your client ID>"
export MICROSTORE_CLIENT_SECRET="<your client key>"

export STORE_ID="<the Store ID of your app>"
```

The tenant ID and the client ID can also be passed via command line options to the client. The secret is always read from the `MICROSTORE_CLIENT_SECRET` environment variable. The Store ID of the app has to be specified on the command line for most commands supported by the client, but it's easier to simply write `${STORE_ID}` on the command line instead of the actual cryptic Store ID.

See [Complete prerequisites for using the Microsoft Store submission API](https://learn.microsoft.com/en-us/windows/uwp/monetize/create-and-manage-submissions-using-windows-store-services#prerequisites) for information how to get your tenant ID, client ID, and client secret.
For more information about the Store ID, see [View app identity details](https://learn.microsoft.com/en-us/windows/apps/publish/view-app-identity-details).

## Development

We recommend using a virtual environment for installing the requirements.

### Setup

```
python3 -m venv .venv
. .venv/bin/activate
pip3 install -r requirements.txt
pip3 install -r requirements_dev.txt
pre-commit install
```

### Run the Client

```
. .venv/bin/activate
. .env
python3 -m microstore --help
```

### Run the Unit Tests, Linters, etc.

```
. .venv/bin/activate
tox
```
