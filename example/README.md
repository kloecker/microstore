<!--
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: 2023 KDE e.V.
# SPDX-FileContributor: Ingo Klöcker <dev@ingo-kloecker.de>
-->

# A Simple Example Client for Submitting an Application Package to the Microsoft Store

## Setup

Create a virtual environment and install the requirements.
```
python3 -m venv .venv
. .venv/bin/activate
pip3 install -r requirements.txt
```

## Run the example client

Define a few environment variables.
```
export MICROSTORE_TENANT_ID="<your tenant ID>"
export MICROSTORE_CLIENT_ID="<your client ID>"
export MICROSTORE_CLIENT_SECRET="<your client key>"
export STORE_ID="<the Store ID of your app>"
```

To submit a new application package to the store, run
```
python3 submitApp.py ${STORE_ID}
```

This will create a new submission with the new application package. All other data (description, images, etc.) is taken from the previous submission. In particular, the first submission has to be made via the [Microsoft Partner Center](https://partner.microsoft.com/en-us/dashboard/windows/overview).
