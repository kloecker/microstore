#!/usr/bin/env python3
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: 2023 KDE e.V.
# SPDX-FileContributor: Ingo Klöcker <dev@ingo-kloecker.de>

import argparse
import json
import logging
import os
import sys
from pathlib import Path
from tempfile import TemporaryDirectory
from typing import Union
from zipfile import ZipFile

try:
    from azure.storage.blob import BlobClient

    from microstore.MicrosoftStoreClient import APIError, AuthorizationError, MicrosoftStoreClient
except ImportError as e:
    print("Error: %s" % e, file=sys.stderr)
    print("Please install the requirements: azure-storage-blob, microstore")
    sys.exit(1)


class Error(Exception):
    pass


logger = logging.getLogger(__name__)


def parseCommandLine() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description="Simple client for submitting apps to the Microsoft Store")

    # debug options
    parser.add_argument("-v", "--verbose", action="count", default=0, help="increase the verbosity")
    parser.add_argument(
        "--dry-run", dest="dryRun", action="store_true", help="perform a trial run without submitting any changes"
    )
    parser.add_argument(
        "--debug-authorization",
        type=int,
        default=0,
        dest="debugAuth",
        metavar="LEVEL",
        help="debug level for authorization (1 or 2)",
    )
    parser.add_argument(
        "--debug-api-calls",
        type=int,
        default=0,
        dest="debugApi",
        metavar="LEVEL",
        help="debug level for API calls (1 or 2)",
    )

    parser.add_argument(
        "--tenant-id",
        dest="tenantId",
        help="the tenant ID; required; can also be set via MICROSTORE_TENANT_ID environment variable",
    )
    parser.add_argument(
        "--client-id",
        dest="clientId",
        help="the client ID; required; can also be set via MICROSTORE_CLIENT_ID environment variable",
    )

    parser.add_argument("storeId", help="the Microsoft Store ID of the application")
    parser.add_argument("appxuploadFile", help="the .appxupload file to submit")

    return parser.parse_args()


def setUpLogging(options: argparse.Namespace) -> None:
    logging.basicConfig(format="%(asctime)s %(name)s %(message)s", level=logging.WARNING)
    logger.setLevel(max(logging.DEBUG, logging.WARNING - 10 * options.verbose))
    microstoreLogger = logging.getLogger("microstore")
    microstoreLogger.setLevel(max(logging.DEBUG, logging.WARNING - 10 * options.verbose))
    authLogger = logging.getLogger("microstore.auth")
    authLogger.setLevel(max(logging.DEBUG, logging.WARNING - 10 * options.debugAuth))
    apiLogger = logging.getLogger("microstore.api")
    apiLogger.setLevel(max(logging.DEBUG, logging.WARNING - 10 * options.debugApi))


def uploadPackagesFiles(zipFilePath: Union[str, Path], fileUploadUrl: str) -> None:
    logger.debug(f"Uploading {zipFilePath} to {fileUploadUrl}")
    blobClient = BlobClient.from_blob_url(fileUploadUrl)
    with open(zipFilePath, "rb") as data:
        result = blobClient.upload_blob(data, blob_type="BlockBlob")
        logger.debug("Upload result: %s", result)


def submitApp(client: MicrosoftStoreClient, storeId: str, appxuploadFile: str, dryRun: bool) -> None:
    if not os.path.isfile(appxuploadFile):
        raise Error(f"File not found: {appxuploadFile}")

    if dryRun:
        logger.info("Running in dry-run mode")

    # get app data
    logger.info("Retrieving information about the app")
    appData = client.getAppData(storeId)

    lastSubmissionId = appData.get("lastPublishedApplicationSubmission", {}).get("id")
    if not lastSubmissionId:
        raise Error("The first submission needs to be done manually in the Microsoft Partner Center.")

    # delete existing pending submission
    pendingSubmissionId = appData.get("pendingApplicationSubmission", {}).get("id")
    if pendingSubmissionId:  # not None and not empty
        logger.info("Deleting existing pending submission")
        if not dryRun:
            client.deleteSubmission(storeId, pendingSubmissionId)

    # create a new submission (as copy of the last submission)
    logger.info("Creating new submission")
    if not dryRun:
        submissionData = client.createSubmission(storeId)
    else:
        submissionData = client.getSubmission(storeId, lastSubmissionId)
        submissionData["id"] = int(submissionData["id"]) + 1
        submissionData["fileUploadUrl"] = "https://localhost"
    submissionId = submissionData["id"]
    logger.debug(f"submission ID: {submissionId}")

    # remove some entries from the submission data
    fileUploadUrl = submissionData["fileUploadUrl"]
    logger.debug(f"fileUploadUrl: {fileUploadUrl}")
    del submissionData["fileUploadUrl"]  # remove fileUploadUrl
    # remove deprecated "pricing.sales" entry
    if "pricing" in submissionData and "sales" in submissionData["pricing"]:
        del submissionData["pricing"]["sales"]

    # add information about the app package (if it has not yet been uploaded)
    appPackageName = os.path.basename(appxuploadFile)
    if any(
        p["fileName"] == appPackageName and p["fileStatus"] == "Uploaded" for p in submissionData["applicationPackages"]
    ):
        # the app package has already been uploaded
        logger.info(f"{appPackageName} has already been uploaded")
        appPackageName = ""
    else:
        logger.info(f"Adding {appPackageName} to list of application packages")
        submissionData["applicationPackages"].append(
            {
                "fileName": appPackageName,  # relative to root of uploaded ZIP file
                "fileStatus": "PendingUpload",
            }
        )

    # update the submission
    logger.info("Updating submission")
    if not dryRun:
        client.updateSubmission(storeId, submissionId, submissionData)
    else:
        print(f"Updated submission data:\n{json.dumps(submissionData, indent=4)}")

    # upload application package in a zip file
    if appPackageName:
        with TemporaryDirectory() as tmpdirname:
            logger.info("Packaging files to upload")
            zipFilePath = Path(tmpdirname) / "blob.zip"
            # we use the default ZIP_STORED compression because the files should already be fairly compressed
            with ZipFile(zipFilePath, "w") as zipFile:
                logger.debug(f"Adding {appPackageName} to {zipFilePath}")
                zipFile.write(appxuploadFile, arcname=appPackageName)

            logger.info("Uploading packaged files")
            if not dryRun:
                uploadPackagesFiles(zipFilePath, fileUploadUrl)

    # commit the submission
    logger.info("Committing submission")
    if not dryRun:
        client.commitSubmission(storeId, submissionId, waitUntilCommitIsCompleted=True)


def main() -> int:
    options = parseCommandLine()
    setUpLogging(options)

    client = MicrosoftStoreClient(options.tenantId, options.clientId)
    try:
        submitApp(client, options.storeId, options.appxuploadFile, options.dryRun)
        return 0
    except AuthorizationError as e:
        print(f"Error: Authentication failed: {e}. Use `--debug-authorization 2` for details.", file=sys.stderr)
    except APIError as e:
        print(f"Error: API request failed: {e}. Use `--debug-api-calls 2` for details.", file=sys.stderr)

    return 1


if __name__ == "__main__":
    try:
        sys.exit(main())
    except Error as e:
        print("Error: %s" % e, file=sys.stderr)
        sys.exit(1)
