# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: 2023 KDE e.V.
# SPDX-FileContributor: Ingo Klöcker <dev@ingo-kloecker.de>

import os
import time
from types import SimpleNamespace
from unittest.mock import Mock

import pytest
import requests

from microstore.MicrosoftStoreClient import APIError, AuthorizationError, MicrosoftStoreClient


@pytest.fixture(autouse=True)
def mock_env_credentials(monkeypatch):
    monkeypatch.setenv("MICROSTORE_TENANT_ID", "tenant")
    monkeypatch.setenv("MICROSTORE_CLIENT_ID", "client")
    monkeypatch.setenv("MICROSTORE_CLIENT_SECRET", "secret")


@pytest.fixture(autouse=True)
def no_requests(monkeypatch):
    """Remove requests.sessions.Session.request for all tests to prevent "requests"
    from performing actual HTTP requests."""
    monkeypatch.delattr("requests.sessions.Session.request")


@pytest.fixture
def mock_requests_post(monkeypatch):
    mock = Mock()
    monkeypatch.setattr(requests, "post", mock)
    return mock


@pytest.fixture
def no_sleep(monkeypatch):
    monkeypatch.setattr(time, "sleep", Mock())


@pytest.fixture
def mock_authorization(monkeypatch):
    mock_accessToken = Mock(return_value="mock-access-token")
    monkeypatch.setattr(MicrosoftStoreClient, "accessToken", property(mock_accessToken))


@pytest.fixture
def mock_api(monkeypatch):
    mocks = SimpleNamespace(
        listApps=Mock(),
        getApp=Mock(),
        createSubmission=Mock(),
        getSubmission=Mock(),
        updateSubmission=Mock(),
        deleteSubmission=Mock(),
        commitSubmission=Mock(),
        getSubmissionStatus=Mock(),
    )

    def apiRequest(self, method, path, **kwargs):
        if not path.startswith("/v1.0/my/applications"):  # pragma: no cover
            raise ValueError(f"Invalid path: {path}")
        parts = path.split("/")[4:]  # split path and throw away the first 4 parts
        appId = parts[0] if len(parts) > 0 else None
        submissionId = parts[2] if len(parts) > 2 else None
        command = parts[3] if len(parts) > 3 else None
        if method == "get":
            if appId is None:
                return mocks.listApps(**kwargs)
            elif submissionId is None:
                return mocks.getApp(appId, **kwargs)
            elif command is None:
                return mocks.getSubmission(appId, submissionId, **kwargs)
            elif command == "status":
                return mocks.getSubmissionStatus(appId, submissionId, **kwargs)
            else:  # pragma: no cover
                raise ValueError(f"Invalid path for GET: {path}")
        elif method == "post":
            if appId is not None and submissionId is None:
                return mocks.createSubmission(appId, **kwargs)
            elif appId is not None and submissionId is not None and command == "commit":
                return mocks.commitSubmission(appId, submissionId, **kwargs)
            else:  # pragma: no cover
                raise ValueError(f"Invalid path for POST: {path}")
        elif method == "put":
            if appId is not None and submissionId is not None:
                return mocks.updateSubmission(appId, submissionId, **kwargs)
            else:  # pragma: no cover
                raise ValueError(f"Invalid path for PUT: {path}")
        elif method == "delete":
            if appId is not None and submissionId is not None:
                return mocks.deleteSubmission(appId, submissionId, **kwargs)
            else:  # pragma: no cover
                raise ValueError(f"Invalid path for DELETE: {path}")
        else:  # pragma: no cover
            raise ValueError(f"Invalid method: {method}")

    monkeypatch.setattr(MicrosoftStoreClient, "apiRequest", apiRequest)
    return mocks


class MockResponse:
    def __init__(self, status_code=200, headers=None, json=None):
        self.status_code = status_code
        self.ok = status_code < 400
        self.text = ""
        self.headers = headers if headers is not None else {}
        self._json = json

    def json(self):
        if self._json == requests.JSONDecodeError:
            raise requests.JSONDecodeError("", "", 0)
        return self._json


class TestMicrosoftStoreClient:
    def test_init_with_credentials_from_environment(self):
        c = MicrosoftStoreClient()
        assert c.tenantId == "tenant"
        assert c.clientId == "client"
        assert c.clientSecret == "secret"

    def test_init_prefers_credentials_passed_as_arguments(self):
        c = MicrosoftStoreClient(tenantId="ttt", clientId="ccc")
        assert c.tenantId == "ttt"
        assert c.clientId == "ccc"
        assert c.clientSecret == "secret"

    def test_tenant_id_is_required(self):
        del os.environ["MICROSTORE_TENANT_ID"]
        with pytest.raises(AttributeError):
            MicrosoftStoreClient()

    def test_client_id_is_required(self):
        del os.environ["MICROSTORE_CLIENT_ID"]
        with pytest.raises(AttributeError):
            MicrosoftStoreClient()

    def test_client_secret_is_required(self):
        del os.environ["MICROSTORE_CLIENT_SECRET"]
        with pytest.raises(AttributeError):
            MicrosoftStoreClient()

    def test_authentication(self, mock_requests_post):
        mock_requests_post.return_value = MockResponse(json={"access_token": "mockAccessToken"})

        c = MicrosoftStoreClient()
        assert c.accessToken == "mockAccessToken"
        assert mock_requests_post.call_count == 1

        assert c.accessToken == "mockAccessToken"
        assert mock_requests_post.call_count == 1, "another POST request was sent unexpectedly"

    def test_authentication_failed_request(self, mock_requests_post):
        mock_requests_post.return_value = MockResponse(status_code=400, json={})

        c = MicrosoftStoreClient()
        with pytest.raises(AuthorizationError, match="HTTP Error"):
            c.accessToken

    def test_authentication_invalid_response(self, mock_requests_post):
        mock_requests_post.return_value = MockResponse(status_code=200, json=requests.JSONDecodeError)

        c = MicrosoftStoreClient()
        with pytest.raises(AuthorizationError, match="Invalid response"):
            c.accessToken

    def test_apiRequest(self, mock_authorization, monkeypatch):
        mock_request = Mock(return_value=MockResponse(json={"id": "app-id"}, headers={}))
        monkeypatch.setattr(requests, "request", mock_request)

        c = MicrosoftStoreClient()
        r = c.apiRequest("method", "/api/endpoint/", foo="bar")

        assert r == {"id": "app-id"}
        assert mock_request.call_count == 1
        assert mock_request.call_args.args[0] == "METHOD"
        assert mock_request.call_args.args[1] == f"{c.apiServer}/api/endpoint/"
        headers = mock_request.call_args.kwargs.get("headers")
        assert headers is not None, "headers not passed to request function"
        assert headers.get("Authorization") == "Bearer mock-access-token"
        foo = mock_request.call_args.kwargs.get("foo")
        assert foo is not None, "keyword argument 'foo' not passed to request function"
        assert foo == "bar", "keyword argument 'foo' not passed as-is to request function"

    def test_apiRequest_failed_request(self, mock_authorization, monkeypatch):
        mock_request = Mock(return_value=MockResponse(status_code=400, json={}, headers={}))
        monkeypatch.setattr(requests, "request", mock_request)

        c = MicrosoftStoreClient()
        with pytest.raises(APIError, match="HTTP Error"):
            c.apiRequest("method", "/api/endpoint/", foo="bar")

    def test_apiRequest_invalid_response(self, mock_authorization, monkeypatch):
        mock_request = Mock(return_value=MockResponse(status_code=200, json=requests.JSONDecodeError, headers={}))
        monkeypatch.setattr(requests, "request", mock_request)

        c = MicrosoftStoreClient()
        with pytest.raises(APIError, match="Invalid response"):
            c.apiRequest("method", "/api/endpoint/", foo="bar")

    def test_apiRequest_allow_empty_response_for_delete(self, mock_authorization, monkeypatch):
        mock_request = Mock(return_value=MockResponse(status_code=204, json=requests.JSONDecodeError, headers={}))
        monkeypatch.setattr(requests, "request", mock_request)

        c = MicrosoftStoreClient()
        r = c.apiRequest("delete", "/api/endpoint/", foo="bar")
        assert r is None

    def test_getAllApps_full_list_on_first_API_request(self, mock_api):
        mock_api.listApps.return_value = {"value": ["a", "b", "c"], "totalCount": 3}

        c = MicrosoftStoreClient()
        r = c.getAllApps()

        assert r == ["a", "b", "c"]
        assert mock_api.listApps.call_count == 1
        assert mock_api.listApps.call_args.args == ()

    def test_getAllApps_partial_list_on_first_API_request(self, mock_api):
        mock_api.listApps.side_effect = (
            {"value": ["a", "b"], "totalCount": 5},
            {"value": ["c", "d", "e"], "totalCount": 5},
        )

        c = MicrosoftStoreClient()
        r = c.getAllApps()

        assert r == ["a", "b", "c", "d", "e"]
        assert mock_api.listApps.call_args_list == [
            (),
            ((), {"params": {"skip": 2, "top": 3}}),
        ]

    def test_getAppData(self, mock_api):
        c = MicrosoftStoreClient()
        c.getAppData("APP-ID")

        assert mock_api.getApp.call_count == 1
        assert mock_api.getApp.call_args.args == ("APP-ID",)

    def test_createSubmission(self, mock_api):
        c = MicrosoftStoreClient()
        c.createSubmission("APP-ID")

        assert mock_api.createSubmission.call_count == 1
        assert mock_api.createSubmission.call_args.args == ("APP-ID",)

    def test_getSubmission(self, mock_api):
        c = MicrosoftStoreClient()
        c.getSubmission("APP-ID", "SUBMISSION-ID")

        assert mock_api.getSubmission.call_count == 1
        assert mock_api.getSubmission.call_args.args == ("APP-ID", "SUBMISSION-ID")

    def test_updateSubmission(self, mock_api):
        c = MicrosoftStoreClient()
        c.updateSubmission("APP-ID", "SUBMISSION-ID", {"foo": "bar"})

        assert mock_api.updateSubmission.call_count == 1
        assert mock_api.updateSubmission.call_args == (
            ("APP-ID", "SUBMISSION-ID"),
            {"json": {"foo": "bar"}},
        )

    def test_deleteSubmission(self, mock_api):
        c = MicrosoftStoreClient()
        c.deleteSubmission("APP-ID", "SUBMISSION-ID")

        assert mock_api.deleteSubmission.call_count == 1
        assert mock_api.deleteSubmission.call_args.args == ("APP-ID", "SUBMISSION-ID")

    def test_getSubmissionStatus(self, mock_api):
        c = MicrosoftStoreClient()
        c.getSubmissionStatus("APP-ID", "SUBMISSION-ID")

        assert mock_api.getSubmissionStatus.call_count == 1
        assert mock_api.getSubmissionStatus.call_args.args == ("APP-ID", "SUBMISSION-ID")

    def test_commitSubmission_without_waiting_for_completion(self, mock_api):
        mock_api.commitSubmission.return_value = {"status": "CommitStarted"}

        c = MicrosoftStoreClient()
        r = c.commitSubmission("APP-ID", "SUBMISSION-ID", waitUntilCommitIsCompleted=False)

        assert r["status"] == "CommitStarted"
        assert mock_api.commitSubmission.call_count == 1
        assert mock_api.commitSubmission.call_args.args == ("APP-ID", "SUBMISSION-ID")

    def test_commitSubmission_with_waiting_for_completion(self, mock_api, no_sleep):
        mock_api.commitSubmission.return_value = {"status": "CommitStarted"}
        mock_api.getSubmissionStatus.side_effect = (
            {"status": "CommitStarted"},
            {"status": "some other status"},
        )

        c = MicrosoftStoreClient()
        r = c.commitSubmission("APP-ID", "SUBMISSION-ID", waitUntilCommitIsCompleted=True)

        assert r["status"] == "some other status"
        assert mock_api.commitSubmission.call_count == 1
        assert mock_api.commitSubmission.call_args.args == ("APP-ID", "SUBMISSION-ID")
        assert mock_api.getSubmissionStatus.call_args_list == [
            (("APP-ID", "SUBMISSION-ID"),),
            (("APP-ID", "SUBMISSION-ID"),),
        ]

    def test_getLastPublishedAppSubmissionData(self, mock_api):
        mock_api.getApp.return_value = {"lastPublishedApplicationSubmission": {"id": "SUBMISSION-ID"}}
        mock_api.getSubmission.return_value = {"foo": "bar"}

        c = MicrosoftStoreClient()
        with pytest.raises(ValueError, match="appId"):
            c.getLastPublishedAppSubmissionData("")
        r = c.getLastPublishedAppSubmissionData("APP-ID")

        assert r == {"foo": "bar"}

    def test_getLastPublishedAppSubmissionData_nothing_published(self, mock_api):
        mock_api.getApp.return_value = {}

        c = MicrosoftStoreClient()
        r = c.getLastPublishedAppSubmissionData("APP-ID")

        assert r is None

    def test_getPendingSubmissionData(self, mock_api):
        mock_api.getApp.return_value = {"pendingApplicationSubmission": {"id": "SUBMISSION-ID"}}
        mock_api.getSubmission.return_value = {"foo": "bar"}

        c = MicrosoftStoreClient()
        with pytest.raises(ValueError, match="appId"):
            c.getPendingSubmissionData("")
        r = c.getPendingSubmissionData("APP-ID")

        assert r == {"foo": "bar"}

    def test_getPendingSubmissionData_no_pending_submission(self, mock_api):
        mock_api.getApp.return_value = {}

        c = MicrosoftStoreClient()
        r = c.getPendingSubmissionData("APP-ID")

        assert r is None

    def test_getPendingSubmissionStatus(self, mock_api):
        mock_api.getApp.return_value = {"pendingApplicationSubmission": {"id": "SUBMISSION-ID"}}
        mock_api.getSubmissionStatus.return_value = {"status": "some status"}

        c = MicrosoftStoreClient()
        with pytest.raises(ValueError, match="appId"):
            c.getPendingSubmissionStatus("")
        r = c.getPendingSubmissionStatus("APP-ID")

        assert r == {"status": "some status"}

    def test_getPendingSubmissionStatus_no_pending_submission(self, mock_api):
        mock_api.getApp.return_value = {}

        c = MicrosoftStoreClient()
        r = c.getPendingSubmissionStatus("APP-ID")

        assert r is None

    def test_commitPendingAppSubmission(self, mock_api, no_sleep):
        mock_api.getApp.return_value = {"pendingApplicationSubmission": {"id": "SUBMISSION-ID"}}
        mock_api.commitSubmission.return_value = {"status": "CommitStarted"}
        mock_api.getSubmissionStatus.return_value = {"status": "some status"}

        c = MicrosoftStoreClient()
        with pytest.raises(ValueError, match="appId"):
            c.commitPendingAppSubmission("")
        r = c.commitPendingAppSubmission("APP-ID")

        assert r == {"status": "some status"}

    def test_commitPendingAppSubmission_no_pending_submission(self, mock_api):
        mock_api.getApp.return_value = {}

        c = MicrosoftStoreClient()
        r = c.commitPendingAppSubmission("APP-ID")

        assert r is None
