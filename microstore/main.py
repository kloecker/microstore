# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: 2023 KDE e.V.
# SPDX-FileContributor: Ingo Klöcker <dev@ingo-kloecker.de>

import argparse
import json
import logging
import sys

from .MicrosoftStoreClient import APIError, AuthorizationError, MicrosoftStoreClient


def parseCommandLine() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        prog=__name__.split(".", maxsplit=1)[0],
        description="Simple client for the Microsoft Store submission API",
    )

    # common options
    parser.add_argument(
        "--tenant-id",
        dest="tenantId",
        help="the tenant ID; required; can also be set via MICROSTORE_TENANT_ID environment variable",
    )
    parser.add_argument(
        "--client-id",
        dest="clientId",
        help="the client ID; required; can also be set via MICROSTORE_CLIENT_ID environment variable",
    )

    # debug options
    parser.add_argument(
        "-v",
        "--verbose",
        action="count",
        default=0,
        help="increase the verbosity",
    )
    parser.add_argument(
        "--debug-authorization",
        type=int,
        default=0,
        dest="debugAuth",
        metavar="LEVEL",
        help="debug level for authorization (1 or 2)",
    )
    parser.add_argument(
        "--debug-api-calls",
        type=int,
        default=0,
        dest="debugApi",
        metavar="LEVEL",
        help="debug level for API calls (1 or 2)",
    )

    # commands
    commandParsers = parser.add_subparsers(
        title="Commands",
        description="Use --help after the command to get additional help for the commands.",
        required=True,
        metavar="command",
    )

    listAppsParser = commandParsers.add_parser(
        "list-apps",
        help="Lists data for all apps registered to your account",
        description="Lists data for all apps registered to your account",
    )
    listAppsParser.set_defaults(func=listApps)

    getAppDataParser = commandParsers.add_parser(
        "get-app-data",
        help="Retrieves information about a specific app",
        description="Retrieves information about a specific app",
    )
    getAppDataParser.add_argument(
        "appId",
        help="the Store ID of the application",
    )
    getAppDataParser.set_defaults(func=getAppData)

    getLastPublishedSubmissionParser = commandParsers.add_parser(
        "get-last-published-submission",
        help="Shows data of the last published app submission",
        description="Shows data of the last published app submission",
    )
    getLastPublishedSubmissionParser.add_argument(
        "appId",
        help="the Store ID of the application",
    )
    getLastPublishedSubmissionParser.set_defaults(func=getLastPublishedAppSubmission)

    getPendingSubmissionParser = commandParsers.add_parser(
        "get-pending-submission",
        help="Shows data of a pending app submission",
        description="Shows data of a pending app submission",
    )
    getPendingSubmissionParser.add_argument(
        "appId",
        help="the Store ID of the application",
    )
    getPendingSubmissionParser.set_defaults(func=getPendingAppSubmission)

    getPendingSubmissionStatusParser = commandParsers.add_parser(
        "get-pending-submission-status",
        help="Shows the status of a pending app submission",
        description="Shows the status of a pending app submission",
    )
    getPendingSubmissionStatusParser.add_argument(
        "appId",
        help="the Store ID of the application",
    )
    getPendingSubmissionStatusParser.set_defaults(func=getPendingAppSubmissionStatus)

    commitSubmissionParser = commandParsers.add_parser(
        "commit-submission",
        help="Commits a pending app submission",
        description="Commits a pending app submission",
    )
    commitSubmissionParser.add_argument(
        "appId",
        help="the Store ID of the application",
    )
    commitSubmissionParser.set_defaults(func=commitPendingAppSubmission)

    return parser.parse_args()


def setUpLogging(options: argparse.Namespace) -> None:
    logging.basicConfig(format="%(asctime)s %(name)s %(message)s", level=logging.WARNING)
    logger = logging.getLogger("microstore")
    logger.setLevel(max(logging.DEBUG, logging.WARNING - 10 * options.verbose))
    authLogger = logging.getLogger("microstore.auth")
    authLogger.setLevel(max(logging.DEBUG, logging.WARNING - 10 * options.debugAuth))
    apiLogger = logging.getLogger("microstore.api")
    apiLogger.setLevel(max(logging.DEBUG, logging.WARNING - 10 * options.debugApi))


def printResult(result: object) -> None:
    print(json.dumps(result, indent=4))


def listApps(client: MicrosoftStoreClient, **kwargs: str) -> None:
    appsData = client.getAllApps()
    printResult(appsData)


def getAppData(client: MicrosoftStoreClient, appId: str, **kwargs: str) -> None:
    appData = client.getAppData(appId)
    printResult(appData)


def getLastPublishedAppSubmission(client: MicrosoftStoreClient, appId: str, **kwargs: str) -> None:
    data = client.getLastPublishedAppSubmissionData(appId)
    printResult(data)


def getPendingAppSubmission(client: MicrosoftStoreClient, appId: str, **kwargs: str) -> None:
    data = client.getPendingSubmissionData(appId)
    printResult(data)


def getPendingAppSubmissionStatus(client: MicrosoftStoreClient, appId: str, **kwargs: str) -> None:
    data = client.getPendingSubmissionStatus(appId)
    printResult(data)


def commitPendingAppSubmission(client: MicrosoftStoreClient, appId: str, **kwargs: str) -> None:
    data = client.commitPendingAppSubmission(appId)
    printResult(data)


def main() -> int:
    options = parseCommandLine()
    setUpLogging(options)

    client = MicrosoftStoreClient(options.tenantId, options.clientId)
    kwargs = {k: v for k, v in vars(options).items() if k in ["appId"]}
    try:
        options.func(client, **kwargs)
        return 0
    except AuthorizationError as e:
        print(f"Error: Authentication failed: {e}. Use `--debug-authorization 2` for details.", file=sys.stderr)
    except APIError as e:
        print(f"Error: API request failed: {e}. Use `--debug-authorization 2` for details.", file=sys.stderr)

    return 1
