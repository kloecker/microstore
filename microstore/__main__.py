# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: 2023 KDE e.V.
# SPDX-FileContributor: Ingo Klöcker <dev@ingo-kloecker.de>

import sys

from .main import main

try:
    sys.exit(main())
except Exception as e:
    print("Error: %s" % e, file=sys.stderr)
    sys.exit(1)
