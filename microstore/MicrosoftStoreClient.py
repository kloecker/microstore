# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: 2023 KDE e.V.
# SPDX-FileContributor: Ingo Klöcker <dev@ingo-kloecker.de>

import json
import logging
import os
import time
from typing import Any, List, Optional

import requests

logger = logging.getLogger("microstore")

authLogger = logging.getLogger("microstore.auth")
authLogger.setLevel(logging.ERROR)

apiLogger = logging.getLogger("microstore.api")
apiLogger.setLevel(logging.ERROR)


class AuthorizationError(requests.HTTPError):
    pass


class APIError(requests.HTTPError):
    pass


class MicrosoftStoreClient:
    apiServer = "https://manage.devcenter.microsoft.com"
    userAgent = "org.kde.microstore"

    def __init__(
        self, tenantId: Optional[str] = None, clientId: Optional[str] = None, clientSecret: Optional[str] = None
    ) -> None:
        super().__init__()
        self._accessToken = ""
        self.tenantId = tenantId or os.environ.get("MICROSTORE_TENANT_ID")
        self.clientId = clientId or os.environ.get("MICROSTORE_CLIENT_ID")
        self.clientSecret = clientSecret or os.environ.get("MICROSTORE_CLIENT_SECRET")
        if not self.tenantId or not self.clientId or not self.clientSecret:
            raise AttributeError("MICROSTORE_TENANT_ID, MICROSTORE_CLIENT_ID, or MICROSTORE_CLIENT_SECRET is missing.")

    @property
    def accessToken(self) -> str:
        if not self._accessToken:
            tokenEndpoint = f"https://login.microsoftonline.com/{self.tenantId}/oauth2/token"
            authLogger.info("Request: POST %s", tokenEndpoint)
            r = requests.post(
                tokenEndpoint,
                data={
                    "grant_type": "client_credentials",
                    "client_id": self.clientId,
                    "client_secret": self.clientSecret,
                    "resource": self.apiServer,
                },
            )
            authLogger.info("Response status: %d", r.status_code)
            try:
                j = r.json()
                authLogger.debug("Response: %s", json.dumps(j))
            except requests.JSONDecodeError:
                j = None
                authLogger.debug("Response: %s", r.text)
            if not r.ok:
                raise AuthorizationError(f"{r.status_code} HTTP Error", response=r)
            if j is None:
                raise AuthorizationError(f"Invalid response: {r.text}", response=r)
            self._accessToken = j["access_token"]
        return self._accessToken

    def apiRequest(self, method: str, path: str, **kwargs: Any) -> Any:
        headers = {
            "Authorization": "Bearer " + self.accessToken,
            "User-Agent": self.userAgent,
        }
        apiEndpoint = f"{self.apiServer}{path}"
        apiLogger.info("Request: %s %s", method.upper(), apiEndpoint)
        if "json" in kwargs:  # pragma: no cover
            apiLogger.debug("with JSON data: %s", json.dumps(kwargs["json"]))
        r = requests.request(method.upper(), apiEndpoint, headers=headers, **kwargs)
        apiLogger.info("Response status: %d", r.status_code)
        apiLogger.info("MS-CorrelationId: %s", r.headers.get("MS-CorrelationId"))
        try:
            j = r.json()
            apiLogger.debug("Response: %s", json.dumps(j))
        except requests.JSONDecodeError:
            j = None
            apiLogger.debug("Response: %s", r.text)
        if not r.ok:
            raise APIError(f"{r.status_code} HTTP Error", response=r)
        if j is None and method.lower() != "delete":
            raise APIError(f"Invalid response: {r.text}", response=r)
        return j

    def getAllApps(self) -> List[Any]:
        responseData = self.apiRequest("get", "/v1.0/my/applications")
        appsData: List[Any] = responseData["value"]
        totalCount = responseData["totalCount"]
        if len(appsData) < totalCount:
            responseData = self.apiRequest(
                "get", "/v1.0/my/applications", params={"skip": len(appsData), "top": totalCount - len(appsData)}
            )
            appsData.extend(responseData["value"])
        return appsData

    def getAppData(self, appId: str) -> Any:
        return self.apiRequest("get", f"/v1.0/my/applications/{appId}")

    def createSubmission(self, appId: str) -> Any:
        return self.apiRequest("post", f"/v1.0/my/applications/{appId}/submissions")

    def getSubmission(self, appId: str, submissionId: str) -> Any:
        return self.apiRequest("get", f"/v1.0/my/applications/{appId}/submissions/{submissionId}")

    def updateSubmission(self, appId: str, submissionId: str, submissionData: object) -> Any:
        return self.apiRequest("put", f"/v1.0/my/applications/{appId}/submissions/{submissionId}", json=submissionData)

    def deleteSubmission(self, appId: str, submissionId: str) -> Any:
        return self.apiRequest("delete", f"/v1.0/my/applications/{appId}/submissions/{submissionId}")

    def getSubmissionStatus(self, appId: str, submissionId: str) -> Any:
        return self.apiRequest("get", f"/v1.0/my/applications/{appId}/submissions/{submissionId}/status")

    def commitSubmission(self, appId: str, submissionId: str, waitUntilCommitIsCompleted: bool = False) -> Any:
        response = self.apiRequest("post", f"/v1.0/my/applications/{appId}/submissions/{submissionId}/commit")
        logger.debug("Status of submission: %s", response["status"])

        if waitUntilCommitIsCompleted:
            # poll submission status until commit process is completed
            while response["status"] == "CommitStarted":
                time.sleep(30)
                response = self.getSubmissionStatus(appId, submissionId)
                logger.debug("Status of submission: %s", response["status"])

        return response

    def getLastPublishedAppSubmissionData(self, appId: str) -> Any:
        if not appId:
            raise ValueError("appId is empty or None")

        # get the app
        logger.info("Retrieving information about the app")
        appData = self.getAppData(appId)

        submissionId = appData.get("lastPublishedApplicationSubmission", {}).get("id")
        if submissionId:  # not None and not empty
            return self.getSubmission(appId, submissionId)

    def getPendingSubmissionData(self, appId: str) -> Any:
        if not appId:
            raise ValueError("appId is empty or None")

        # get the app
        logger.info("Retrieving information about the app")
        appData = self.getAppData(appId)

        pendingSubmissionId = appData.get("pendingApplicationSubmission", {}).get("id")
        if pendingSubmissionId:  # not None and not empty
            return self.getSubmission(appId, pendingSubmissionId)

    def getPendingSubmissionStatus(self, appId: str) -> Any:
        if not appId:
            raise ValueError("appId is empty or None")

        # get the app
        logger.info("Retrieving information about the app")
        appData = self.getAppData(appId)

        pendingSubmissionId = appData.get("pendingApplicationSubmission", {}).get("id")
        if pendingSubmissionId:  # not None and not empty
            return self.getSubmissionStatus(appId, pendingSubmissionId)

    def commitPendingAppSubmission(self, appId: str) -> Any:
        if not appId:
            raise ValueError("appId is empty or None")

        # get the app
        logger.info("Retrieving information about the app")
        appData = self.getAppData(appId)

        pendingSubmissionId = appData.get("pendingApplicationSubmission", {}).get("id")
        if not pendingSubmissionId:  # None or empty
            logger.error("Error: There is no pending app submission.")
            return

        # commit the pending app submission
        logger.info("Committing submission")
        return self.commitSubmission(appId, pendingSubmissionId, waitUntilCommitIsCompleted=True)
